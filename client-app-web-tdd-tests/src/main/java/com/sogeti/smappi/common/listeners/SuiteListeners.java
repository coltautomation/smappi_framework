package com.sogeti.smappi.common.listeners;

import org.testng.ISuite;
import org.testng.ISuiteListener;

import com.sogeti.smappi.common.utils.DriverManagerUtil;
/**
 * Suite listener
 * @author Savita Tambe
 *
 */
public class SuiteListeners implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
    	//Start Selenium Grid
    	DriverManagerUtil.startSeleniumGrid();
    	
		//Database connection
		//DBConnectionUtil.oracleDBConnector();
    }
    
    @Override
    public void onFinish(ISuite suite) {    	
    	//DBConnectionUtil.closeDB();	
    }
}