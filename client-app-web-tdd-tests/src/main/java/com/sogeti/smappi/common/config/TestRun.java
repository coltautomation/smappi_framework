package com.sogeti.smappi.common.config;

/**
 * Class to declare constants
 * @author Savita Tambe
 *
 */
public class TestRun {		
	private static ThreadLocal<String> environment = new ThreadLocal<String>();
	private static ThreadLocal<String> platformName = new ThreadLocal<String>();
	private static ThreadLocal<String> platformVersion = new ThreadLocal<String>();
	private static ThreadLocal<String> browserName = new ThreadLocal<String>();	
	private static ThreadLocal<String> browserVersion = new ThreadLocal<String>();	
	
	//SauceLabs
	private static ThreadLocal<String> name = new ThreadLocal<>();
	
    	
    public static void setEnvironment(String environment) {
        TestRun.environment.set(environment);
    }
    
    public static String getEnvironment() {
    	return environment.get();
    }
    
    public static Boolean isLocal() {
    	return getEnvironment().equalsIgnoreCase("Local");
    }
    
    public static Boolean isBrowserStack() {
    	return getEnvironment().equalsIgnoreCase("BrowserStack");
    }
    
    public static Boolean isSauceLabs() {
    	return getEnvironment().equalsIgnoreCase("SauceLabs");
    }
    
    public static Boolean isPerfecto() {
    	return getEnvironment().equalsIgnoreCase("Perfecto");
    }
    
    public static Boolean isSeeTest() {
    	return getEnvironment().equalsIgnoreCase("SeeTest");
    }     
   
    public static void setPlatformName(String platformName) {
        TestRun.platformName.set(platformName);
    }
    
    public static String getPlatformName() {
        return TestRun.platformName.get();
    }  
    
    public static void setPlatformVersion(String platformVersion) {
        TestRun.platformVersion.set(platformVersion);
    }
    
    public static String getPlatformVersion() {
        return TestRun.platformVersion.get();
    } 
    
    public static void setBrowserName(String browserName) {
        TestRun.browserName.set(browserName);
    }
    
    public static String getBrowserName() {
        return TestRun.browserName.get();
    }    
    
    public static void setBrowserVersion(String browserVersion) {
        TestRun.browserVersion.set(browserVersion);
    }
    
    public static String getBrowserVersion() {
        return TestRun.browserVersion.get();
    }  
    
    public static void setName(String name) {
        TestRun.name.set(name);
    }
    
    public static String getName() {
        return TestRun.name.get();
    }
}