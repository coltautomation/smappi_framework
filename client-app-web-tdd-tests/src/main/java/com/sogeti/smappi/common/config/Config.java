package com.sogeti.smappi.common.config;
/**
 * Class to declare constants
 * @author Savita Tambe
 *
 */
public class Config {
	//Allure Screenshot capture
	public static final String SCREENSHOT_CAPTURE = "EveryStep"; //EveryStep or LastStep
		
	//Grid Config
	public static final String GRID_HUB_IP = "localhost";
	public static final String GRID_HUB_PORT = "4443";
	
	//Browser Stack
	public static final String BROWSER_STACK_USER_NAME = ""; 
	public static final String BROWSER_STACK_ACCESS_KEY = ""; 
	public static final String BROWSER_STACK_URL = "";	
	
	//SauceLabs details
	public static final String SAUCELABS_USER_NAME = "";
	public static final String SAUCELABS_ACCESS_KEY = "";
	public static final String SAUCELABS_URL = "";	
		
	//Perfecto details
	public static final String PERFECTO_USER_NAME = ""; 
	public static final String PERFECTO_SECURITY_TOKEN = ""; 
	public static final String PERFECTO_URL = "";	
	
	//SeeTest
	public static final String SEETEST_ACCESS_KEY = "";
	public static final String SEETEST_URL = "";		
		
	//RemoteWebdriver Config
	public static final int XSMALL_PAUSE = 3;
	public static final int SMALL_PAUSE = 10;
	public static final int MEDIUM_PAUSE = 30;
	public static final int LARGE_PAUSE = 60;
	public static final int XLARGE_PAUSE = 120;
	public static final int POLLING_TIME = 500; 
	 
	//File Paths
	public static final String SELENIUM_GRID_FILE_PATH = System.getProperty("user.dir") + "\\src\\main\\resources\\grid\\SeleniumGrid.bat";
	public static final String TEST_DATA_FILE_PATH = System.getProperty("user.dir")+"\\src\\test\\resources\\testdata\\TestData.xlsx";
	
	//App Config
	public static final String APP_URL = "https://opensource-demo.orangehrmlive.com";
	
	//App specific DB Config	
	public static final String DB_HOST = "";
	public static final String DB_PORT = "";
	public static final String DB_SID = "";
	public static final String DB_USER_ID = "";
	public static final String DB_PASS = "";		
}