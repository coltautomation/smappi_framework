package com.sogeti.smappi.common.utils;

import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.sogeti.smappi.common.config.Config;
import com.sogeti.smappi.common.config.TestRun;

/**
 * Class to set desired capabilities
 * @author Savita Tambe
 *
 */
public class CapabilityFactoryUtil {	
	/**********************************************************************************************
	 * Sets the desired capability
	 * 
	 * @author Savita Tambe created March 27, 2018
	 * @version 1.0 March 27, 2018
	 ***********************************************************************************************/	
    public static void initiateDriver() {
    	DesiredCapabilities capabilities = new DesiredCapabilities();	
    	
		switch(TestRun.getEnvironment().toUpperCase())
    	{
    	   case "LOCAL" :
    	   {    	    		       		   
    		   if (TestRun.getBrowserName().equalsIgnoreCase("Chrome")) {    	  
					String path = System.getProperty("user.dir")+"\\src\\test\\resources\\downloads";
		   					   	     
					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					chromePrefs.put("profile.default_content_settings.popups", 0);
					chromePrefs.put("download.default_directory", path);
					 
					ChromeOptions options = new ChromeOptions();
					HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
					options.setExperimentalOption("prefs", chromePrefs);
					options.addArguments("--test-type");
					options.addArguments("--disable-extensions");
					options.addArguments("start-maximized");
					 
					capabilities.setBrowserName("chrome");
					capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);              
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);	           
    		   }
	         
	     	   else if (TestRun.getBrowserName().equalsIgnoreCase("IE")) {   
    	            capabilities.setBrowserName("internet explorer");
    	         	capabilities.setCapability("requireWindowFocus", false);
    	         	capabilities.setCapability("javascriptEnabled", true);
    	         	capabilities.setCapability("ensureCleanSession", true);
    	           	          	
    	            capabilities.setCapability("nativeEvents", false);
    	           	capabilities.setCapability("unexpectedAlertBehaviour", "accept");
    	           	capabilities.setCapability("ignoreProtectedModeSettings", true);
    	           	capabilities.setCapability("disable-popup-blocking", true);
    	           	capabilities.setCapability("enablePersistentHover", true);
    	           	capabilities.setCapability("ignoreZoomSetting", true);
    	         	
    	       }
	         
	     	   else if (TestRun.getBrowserName().equalsIgnoreCase("Firefox")) {
    	         	capabilities.setBrowserName("firefox");    	             	     
	     	   }
    	     	
	     	   break;
    		}

    	   	case "BROWSERSTACK" :
    	   	{
    			capabilities.setCapability("os", TestRun.getPlatformName());
    			capabilities.setCapability("os_version", TestRun.getPlatformVersion());
    			capabilities.setCapability("browser", TestRun.getBrowserName());
    			capabilities.setCapability("browser_version", TestRun.getBrowserVersion());
    			capabilities.setCapability("browserstack.local", "false");
    			capabilities.setCapability("browserstack.selenium_version", "3.5.2");
    			
    			break;
    		}
    	   	
    	    case "SAUCELABS" :
    	    {   
    			if (TestRun.getBrowserName().equalsIgnoreCase("Chrome"))   
	    			capabilities = DesiredCapabilities.chrome();
    			
    			else if (TestRun.getBrowserName().equalsIgnoreCase("Firefox"))  
	    			capabilities = DesiredCapabilities.firefox();
	    			
	    		else if (TestRun.getBrowserName().equalsIgnoreCase("IE"))
	    			capabilities = DesiredCapabilities.internetExplorer();
		    			
	    		capabilities.setCapability("name", TestRun.getName());
	    		capabilities.setCapability("platform", TestRun.getPlatformName());
   	           	capabilities.setCapability("version", TestRun.getBrowserVersion());
	    		
	    		break;
    	    }
    	    
    	    case "PERFECTO" :
    	   	{
    			capabilities.setCapability("securityToken", Config.PERFECTO_SECURITY_TOKEN);
    			capabilities.setCapability("platformName", TestRun.getPlatformName());
    			capabilities.setCapability("platformVersion", TestRun.getPlatformVersion());
    			capabilities.setCapability("browserName", TestRun.getBrowserName());
    			capabilities.setCapability("browserVersion", TestRun.getBrowserVersion());
    			capabilities.setCapability("resolution", "1280x1024");
    			capabilities.setCapability("location", "US East");
	    		
    			break;
    	   	}
	
    	   	case "SEETEST" :
    	   	{
	    		capabilities.setCapability("accessKey", Config.SEETEST_ACCESS_KEY);  
	    		capabilities.setCapability("os", TestRun.getPlatformName());
    			capabilities.setCapability("browser", TestRun.getBrowserName());
    			capabilities.setCapability("browserVersion", TestRun.getBrowserVersion());	    			
    			
    			break;
    	   	} 
    	   	
    	   	default:
    	   		LoggerUtil.logMessage("Please specify the environment for execution.");
    	}
		
    	//Driver initialization				
		DriverManagerUtil.initiateWebDriver(capabilities);
    }  
}