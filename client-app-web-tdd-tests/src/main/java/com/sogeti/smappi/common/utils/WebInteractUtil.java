package com.sogeti.smappi.common.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sogeti.smappi.common.config.Config;


/**
 * Class having web web application specific reusable methods
 * @author Savita Tambe
 *
 */
public class WebInteractUtil {	    
	/**********************************************************************************************
	 * Waits for web element and clicks on it
	 * 
	 * @param url {@link String} - Url to launch
	 * @author Savita Tambe created May 31, 2018
	 * @version 1.0 May 31, 2018
	 ***********************************************************************************************/
	public static void launchWebApp(String url) {		
		DriverManagerUtil.getWebDriver().navigate().to(url);		
    	try {
    			DriverManagerUtil.getWebDriver().manage().window().maximize();
    	} catch(Exception e) {
            LoggerUtil.logErrorMessage("Could not launch application: " + url);
    	}
    	
        LoggerUtil.logMessage("Launched application: " + url);
    }
	    
	/**********************************************************************************************
	 * Waits for web element and clicks on it
	 * 
	 * @param webElement {@link WebElement} - WebElement to click
	 * @return status {@link boolean} - true/false
	 * @author Savita Tambe created May 31, 2018
	 * @version 1.0 May 31, 2018
	 ***********************************************************************************************/
    public static boolean click(WebElement webElement) {
    	boolean status = false;
    	
    	try {
    			scrollIntoView(webElement);    			
    			webElement.click();
    			status = true;
    			
		} catch (StaleElementReferenceException e1) {
			for (int i = 0; i <= 10; ++i) {
				try {
						waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
		    			webElement.click();					
						break;
				} catch (Exception e) {
					continue;
				}	
			}
		} catch (Exception e) {
			LoggerUtil.logErrorMessage("Unable to click webelement: " + webElement.toString());
		}
		
		return status;
	}
    
    /**********************************************************************************************
     * Clicks the identified web element by javascript.
     * 
     * @param webElement {@link WebElement} - WebElement to click
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 11, 2018
     * @version 1.0 May 11, 2018
     ***********************************************************************************************/
     public static boolean clickByJS(WebElement webElement) {
    	 boolean status = false;     
    	 JavascriptExecutor js = null;
    	 
    	 try {
    		 	waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
    		 	js = (JavascriptExecutor) DriverManagerUtil.getWebDriver();    	    
     			js.executeScript("arguments[0].click();", webElement);
     			status = true;
     			
	     } catch (Exception e) {
				LoggerUtil.logErrorMessage("Unable to clickByJS webelement: " + webElement.toString());
	     }
		 
		 return status;
     }
     
    /**********************************************************************************************
     * Waits for web element and set text in it
     * 
     * @param webElement {@link WebElement} - WebElement to enter text
     * @param text {@link String} - Text to enter
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 31, 2018
     * @version 1.0 May 31, 2018
     ***********************************************************************************************/
    public static boolean sendKeys(WebElement webElement, String text) {
    	boolean status = false;
      	
    	try {
    			waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
				webElement.clear();
				webElement.sendKeys(text);
				status = true;
		} catch (Exception e) {
			LoggerUtil.logErrorMessage("Unable to set text in webelement: " + webElement.toString());
		}
		
		return status;
	}
    
    /**********************************************************************************************
     * Sends text in element with javascript.
     * 
     * @param webElement {@link WebElement} - WebElement to click
     * @param value {@link String} - Value to be entered
     * @author Savita Tambe created May 11, 2018
     * @version 1.0 May 11, 2018
     ***********************************************************************************************/
    public static void sendKeysByJS(WebElement webElement, String value) {
    	JavascriptExecutor js = null;
   		
    	js = (JavascriptExecutor) DriverManagerUtil.getWebDriver();
    	js.executeScript("arguments[0].value='"+ value +"';", webElement);
    }
    
    
    /**********************************************************************************************
     * Waits for web element and selects value from drop down
     * 
     * @param webElement {@link WebElement} - WebElement to select value
     * @param value {@link String} - Value to select
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 31, 2018
     * @version 1.0 May 31, 2018
     ***********************************************************************************************/
    public static boolean selectByValue(WebElement webElement, String value) {
    	boolean status = false;
      	
    	try {
    			waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);	
				Select listBox = new Select(webElement);
				listBox.selectByValue(value);			
				status = true;
				
		} catch (Exception e) {
			LoggerUtil.logErrorMessage("Unable to select the value from listbox: " + webElement.toString());
		}
		
		return status;
	}
    
    /**********************************************************************************************
     * Waits for web element and selects value from drop down
     * 
     * @param webElement {@link WebElement} - WebElement to select value
     * @param value {@link String} - Value to select
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 31, 2018
     * @version 1.0 May 31, 2018
     ***********************************************************************************************/
    public static boolean selectByVisibleText(WebElement webElement, String value) {
    	boolean status = false;
      	
    	try {
    			waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);	
				Select listBox = new Select(webElement);
				listBox.selectByVisibleText(value);			
				status = true;
				
		} catch (Exception e) {
			LoggerUtil.logErrorMessage("Unable to select the value from listbox: " + webElement.toString());
		}
		
		return status;
	}
    
    /**********************************************************************************************
     * Waits for web element and selects value from drop down
     * 
     * @param webElement {@link WebElement} - WebElement to select value
     * @param index {@link int} - Value index to select
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 31, 2018
     * @version 1.0 May 31, 2018
     ***********************************************************************************************/
    public static boolean selectByIndex(WebElement webElement, int index) {
    	boolean status = false;
		try {
				waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);		
				Select listBox = new Select(webElement);
				listBox.selectByIndex(index);
				status = true;
				
		} catch (Exception e) {
			LoggerUtil.logErrorMessage("Unable to select the value from listbox: " + webElement.toString());
		}
		
		return status;
	}
    
    /**********************************************************************************************
     * Waits for web element and clears text in it
     * 
     * @param webElement {@link WebElement} - WebElement to clear text
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 31, 2018
     * @version 1.0 May 31, 2018
     ***********************************************************************************************/
    public static boolean clear(WebElement webElement) {
    	boolean status = false;
		try {
				waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
				webElement.clear();
				status = true;
		} catch (Exception e) {
			LoggerUtil.logErrorMessage("Unable to clear text in webelement: " + webElement.toString());
		}
		
		return status;
	}     
    
     
    /**********************************************************************************************
     * Waits for web element visibility
     * 
     * @param webElement {@link WebElement} - WebElement to wait for visibility
     * @param timeOut {@link int} - The amount of time in milliseconds to pause
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 31, 2018
     * @version 1.0 May 31, 2018
     ***********************************************************************************************/
    public static boolean waitForElementToBeVisible(WebElement webElement, int timeOut) {
    	boolean status = false;
    	
    	try {							
    			FluentWait<RemoteWebDriver> fluentWait = new FluentWait<>(DriverManagerUtil.getWebDriver())    		     
    				.withTimeout(timeOut, TimeUnit.SECONDS)
    		        .pollingEvery(Config.POLLING_TIME, TimeUnit.MILLISECONDS)
    		        .ignoring(NoSuchElementException.class);   
    			
    				fluentWait.until(ExpectedConditions.elementToBeClickable(webElement));	
    				status = true;
		} catch (Exception e) {
			//TODO Auto-generated method stub
		}
		
		return status;
	}
    
    /**********************************************************************************************
     * Waits for web element to be non visible
     * 
     * @param webElement {@link WebElement} - WebElement to wait for non visibility
     * @param timeOut {@link int} - The amount of time in milliseconds to pause
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 31, 2018
     * @version 1.0 May 31, 2018
     ***********************************************************************************************/
    public static boolean waitForInvisibilityOfElement(WebElement webElement, int timeOut) {
    	boolean status = false;
    	
    	try {	  	
    			FluentWait<RemoteWebDriver> fluentWait = new FluentWait<>(DriverManagerUtil.getWebDriver())    		       
					.withTimeout(timeOut, TimeUnit.SECONDS)
    		        .pollingEvery(Config.POLLING_TIME, TimeUnit.MILLISECONDS)
    		        .ignoring(NoSuchElementException.class);
    			
    				fluentWait.until(ExpectedConditions.invisibilityOf(webElement));	
    				status = true;
							
		} catch (Exception e) {
			LoggerUtil.logErrorMessage("Webelement is present: " + webElement.toString());
		}
		
		return status;
	}
    
    /**********************************************************************************************
     * Verifies element is present
     * 
     * @param webElement {@link WebElement} - WebElement to wait for visibility
     * @param timeOut {@link Integer} - The amount of time in milliseconds to pause.
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 30, 2018 
     * @version 1.0 May 30, 2018   
     ***********************************************************************************************/    
   	public static boolean isPresent(WebElement webElement, int timeOut) {
	   	boolean status = false;
	   	
	   	waitForElementToBeVisible(webElement, timeOut);   
	   	try {
	   			status = webElement.isDisplayed();
	   	} catch(Exception e) {
			LoggerUtil.logErrorMessage("Webelement is not present: " + webElement.toString());
	   	}
			  
	    return status;
   	} 
   	 
   	/**********************************************************************************************
     * Verifies element is enabled
     * 
     * @param webElement {@link WebElement} - WebElement to verify if enabled
     * @return status {@link boolean} - true/false
     * @author Savita Tambe created May 30, 2018 
     * @version 1.0 May 30, 2018
     ***********************************************************************************************/    
   	public static boolean isEnabled(WebElement webElement) {
	   	boolean status = false;
	   	
	   	waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);   
	   	try {
	   			status = webElement.isEnabled();				 		    
	   	} catch(Exception e) {
			LoggerUtil.logErrorMessage("Webelement is disabled: " + webElement.toString());
	   	}
		  
       return status;
   	} 
    
    /**********************************************************************************************
    * Determines if an element has a specific text value or not.
    * 
    * @param webElement {@link WebElement} - WebElement to verify text 
    * @param text {@link String} - Text to evaluate
    * @return status {@link boolean} - true/false
    * @author Savita Tambe created Maay 08, 2018 
    * @version 1.0 May 08, 2018
    ***********************************************************************************************/
    public static boolean verifyText(WebElement webElement, String text) {
	   	boolean status = false;
	   	
	   	waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
	   	try { 	   		
	   			status = webElement.getText().equalsIgnoreCase(text);  
        } catch (Exception e) {        	                
			LoggerUtil.logErrorMessage("Unable to verify text for webelement: " + webElement.toString());
        }
	   	
        return status;
    }
    
    /**********************************************************************************************
     * Fetches elements specific attribute value
     * 
     * @param webElement {@link WebElement} - WebElement to get attribute
     * @param attribute {@link String} - The specific attribute type to fetch value   
     * @return attributeValue {@link String} - The specific attribute value   
     * @author Savita Tambe created May 30, 2018 
     * @version 1.0 May 30, 2018     
     ***********************************************************************************************/
     public static String getAttribute(WebElement webElement, String attribute) {
    	String attributeValue = "";   
    	
    	waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
 	   	try {
 	   			attributeValue = webElement.getAttribute(attribute);
         } catch (Exception e) {
        	 LoggerUtil.logErrorMessage("Unable to get attribute for webelement: " + webElement.toString());
         }
 	   	
         return attributeValue;
     }
        
     /**********************************************************************************************
	 * Determines if an element has a specific attribute value or not.
	 * 
	 * @param webElement {@link WebElement} - WebElement to verify attribute
	 * @param attribute {@link String} - The specific attribute type to evaluate
	 * @param attributeValue {@link String} - The value of the attribute to evaluate
	 * @return status {@link boolean} - true/false
	 * @author Savita Tambe created May 30, 2018 
	 * @version 1.0 May 30, 2018
	 ***********************************************************************************************/
     public static boolean verifyAttributeValue(WebElement webElement, String attribute, String attributeValue) {
      	boolean status = false;
      	
      	waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
   	   	try { 	   		
   	   			status = webElement.getAttribute(attribute).equalsIgnoreCase(attributeValue);  
           } catch (Exception e) {        	                
        	   LoggerUtil.logErrorMessage("Unable to get attribute for webelement: " + webElement.toString());
           }
   	   	
           return status;
      }
       
     /**********************************************************************************************
      * Determines if an element has a specific attribute value or not.
      * 
      * @param webElement {@link WebElement} - WebElement to verify attribute
      * @param attribute {@link String} - The specific attribute type to evaluate
      * @param attributeValue {@link String} - The value of the attribute to evaluate
      * @return status {@link boolean} - true/false
      * @author Savita Tambe created May 30, 2018 
      * @version 1.0 May 30, 2018
      ***********************************************************************************************/
     public static boolean verifyAttributeContains(WebElement webElement, String attribute, String attributeValue) {
    	 boolean status = false;   

    	 waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);    	
    	 try {
		  		status = webElement.getAttribute(attribute).toUpperCase().contains(attributeValue.toUpperCase());  	               	   	 	
	   	 } catch (Exception e) {  	   		  
	   		 LoggerUtil.logErrorMessage("Unable to verify attribute contains for webelement: " + webElement.toString());
	   	 }
        return status;
    }
     
	 /**********************************************************************************************
	  * Waits for element to have specific attribute value.
	  * 
	  * @param webElement {@link WebElement} - WebElement to verify attribute
	  * @param attribute {@link String} - The specific attribute type to evaluate
	  * @param attributeValue {@link String} - The value of the attribute to evaluate
	  * @param timeOut {@link int} - The value of the timeout
	  * @return status {@link boolean} - true/false
	  * @author Savita Tambe created May 30, 2018 
	  * @version 1.0 May 30, 2018
	  ***********************************************************************************************/
     public static boolean waitForAttributeContains(WebElement webElement, String attribute, String attributeValue, int timeOut) {
	    boolean status = false;   
	          
	    try {
	          WebDriverWait wait = new WebDriverWait(DriverManagerUtil.getWebDriver(), timeOut);
	          wait.until(ExpectedConditions.attributeContains(webElement, attribute, attributeValue));                  
	          status = true;
              
       } catch (Exception e) {                  
              LoggerUtil.logErrorMessage("Unable to verify attribute contains for webelement: " + webElement.toString());
       }
	    return status;
	 } 
	 
	 /**********************************************************************************************
	  * Scroll to element
	  * 
	  * @param webElement {@link WebElement} - WebElement to verify attribute
	  * @return status {@link boolean} - true/false
	  * @author Savita Tambe created May 30, 2018 
	  * @version 1.0 May 30, 2018
	  ***********************************************************************************************/
	 public static boolean scrollIntoView(WebElement webElement) {
		 boolean status = false;
	    	
		 try {
			 	waitForElementToBeVisible(webElement, Config.MEDIUM_PAUSE);
			 	((JavascriptExecutor) DriverManagerUtil.getWebDriver()).executeScript("arguments[0].scrollIntoView();", webElement);
				
			 } catch (Exception e) {
				LoggerUtil.logErrorMessage("Unable to scroll to webelement: " + webElement.toString());
			 }
			
		 return status;
	 }
		    
	 /**********************************************************************************************
	  * Switch to frame
	  * 
	  * @param idNameIndex {@link String} - Id, Name or Index of the frame
	  * @return status {@link boolean} - true/false
	  * @author Savita Tambe created May 30, 2018 
	  * @version 1.0 May 30, 2018
	  ***********************************************************************************************/
	 public static boolean switchToFrame(String idNameIndex) {
		 boolean status = false;
		 try {
	              DriverManagerUtil.getWebDriver().switchTo().frame(idNameIndex);					 		            
	              status = true;
		 } catch (Exception e) {
			 LoggerUtil.logErrorMessage("Unable to switch to frame: " + idNameIndex);
		 }

       return status;
	  }
	 
	 /**********************************************************************************************
     * Pauses the test action.
     * 
     * @param waitTime {@link Integer} - The amount of time in milliseconds to pause.
     * @author Savita Tambe created March 30, 2018 
     * @version 1.0 March 30, 2018
     ***********************************************************************************************/
	 public static void pause(Integer waitTime) {
	    try {
	        	Thread.sleep(waitTime);
	    } catch (Exception e) {
	    	//TODO Auto-generated method stub
	    }
	 }	
}