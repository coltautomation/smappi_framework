package com.sogeti.smappi.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.sogeti.smappi.common.config.Config;

/**
 * Class for Excel handling methods
 * @author Savita Tambe
 *
 */
public class ExcelUtil {
	private static XSSFSheet excelWSheet;
	private static XSSFWorkbook excelWBook;	
	
	/**********************************************************************************************
	* To set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method
	*   
	* @param filePath {@link String} - TestData file name	
	* @param sheetName {@link String} - Sheet name
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/
	public static void setExcelFile(String filePath, String sheetName) {
	   try {
				FileInputStream excelFile = new FileInputStream(filePath);
				excelWBook = new XSSFWorkbook(excelFile);
				excelWSheet = excelWBook.getSheet(sheetName);
		
			} catch (Exception e){
				LoggerUtil.logErrorMessage("Error while opening TestData file: " + e);
			}	
	}
	
	/**********************************************************************************************
	* To read the test data from the Excel cell, in this we are passing parameters as Row num and Col num
	*   
	* @param rowNo {@link int} - Row number
	* @param colNo {@link int} - Column number
	* @return cellData {@link String} - Cell value
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/
	public static String getCellData(int rowNo, int colNo) {
		 String cellData = null;
		 
		 try {
				 FormulaEvaluator evaluator = excelWBook.getCreationHelper().createFormulaEvaluator();
				 XSSFCell cell = excelWSheet.getRow(rowNo).getCell(colNo);
				 cell.setCellType(CellType.STRING);
				 CellValue cellValue = evaluator.evaluate(cell); 
				 cellData = cellValue.getStringValue();
				 if (cellData.isEmpty())
					 cellData = ""; 
				 
		 } catch (Exception e) {
			 cellData = "";
		 }
	
		 return cellData;
	}
	
	/**********************************************************************************************
	* To set the test data in the Excel cell, in this we are passing parameters as Row num and Col num
	*   
	* @param rowNo {@link int} - Row number
	* @param colNo {@link int} - Column number
	* @param obj {@link Object} - Value to be updated in Excel
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static void setCellData(int rowNo, int colNo, Object obj) {
		Cell cell = excelWSheet.getRow(rowNo).createCell(colNo);
		
		 if(obj instanceof Date) 
			 cell.setCellValue((Date)obj);
		 else if(obj instanceof Boolean)
			 cell.setCellValue((Boolean)obj);
		 else if(obj instanceof String)		        
			 cell.setCellValue((String)obj);		          				        		         
		 else if(obj instanceof Integer)
			 cell.setCellValue((Integer)obj);
		 else if(obj instanceof Double)
			 cell.setCellValue((Double)obj);		      					        	
	}
	
	/**********************************************************************************************
	* Get first row no of TestCased Id
	*   
	* @param testCaseId {@link int} - TestCase Id
	* @param colNo {@link int} - Column number
	* @return rowNo {@link int} - Row number
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static int getFirstRowContainsTestCaseId(String testCaseId, int colNo){
		int rowNo;
		int rowCount = excelWSheet.getLastRowNum();
		for (rowNo=1;rowNo<=rowCount;rowNo++) {
			if (getCellData(rowNo, colNo).equalsIgnoreCase(testCaseId))
				break;
		}
		
		return rowNo;
	 }

	/**********************************************************************************************
	*  Get last row no of TestCased Id
	*   
	* @param testCaseId {@link String} - TestCase Id	
	* @param colNo {@link int} - Column number	
	* @return rowNo {@link int} - Row number
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static int getLastRowContainsTestCaseId(String testCaseId, int colNo){
		int rowNo;
		int startIteration;
		int endIteration = 0;
		
		int rowCount = excelWSheet.getLastRowNum();
		startIteration = getFirstRowContainsTestCaseId(testCaseId, colNo);
		
		for (rowNo=startIteration;rowNo<=rowCount;rowNo++) {
			if (getCellData(rowNo, colNo).equalsIgnoreCase(testCaseId)) 
				endIteration = rowNo;
			else
				break;
		}
		
		return endIteration;
	 }
	
	/**********************************************************************************************
	* To fetch colno based on TestCase Name existance
	*   	
	* @param colName {@link String} - Column name
	* @return colNum {@link int} - Column number
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/
	public static int getColContains(String colName){
		int colNo;
		int colCount = excelWSheet.getRow(0).getLastCellNum();
		
		for (colNo=0;colNo<colCount;colNo++){
			if(getCellData(0,colNo).equals(colName)){
				break;
			}
		}
	
		return colNo;
	 }
	 
	/**********************************************************************************************
	* To get the module name
	*   	
	* @param className {@link String} - Class name
	* @return moduleName {@link String} - Module number
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/		
	public static String getModuleName(String className) {
		return className.split("_")[0];
	}
	
	/**********************************************************************************************
	* To get the Test Case Id
	*   	
	* @param className {@link String} - Class name
	* @return testCaseID {@link String} - Test Case Id
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static String getTestCaseId(String className) {
		return (className.split("_")[1]);
	}
	
	/**********************************************************************************************
	* To fetch the test data
	*   	
	* @param className {@link String} - Class name
	* @return testData {@link Map} - Hash map
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static HashMap<Integer, HashMap<String, String>> getTestData(String className) {
		String sheetName;
		String testCaseId;
		int startIteration;
		int endIteration;
		int startCol = 2;
		int totalCols;
		
		HashMap<String, String> testData = null;
		HashMap<Integer, HashMap<String, String>> testDataIterations = new HashMap<Integer, HashMap<String, String>>();
				
		sheetName = getModuleName(className);
	    testCaseId = getTestCaseId(className);
		
		setExcelFile(Config.TEST_DATA_FILE_PATH, sheetName);
		startIteration = getFirstRowContainsTestCaseId(testCaseId, 0);
		endIteration = getLastRowContainsTestCaseId(testCaseId, 0);
		
		totalCols = excelWSheet.getRow(0).getLastCellNum();
		
		int iteration = 1;
		for (int i=startIteration;i<=endIteration;i++) {
			if (getCellData(i,2).equalsIgnoreCase("Y")) {
				testData = new HashMap<String, String>();

				for (int j=startCol;j<totalCols;j++)				
				{				
					testData.put(getCellData(startIteration-1,j), getCellData(i,j));
			    }
				
				testDataIterations.put(iteration, testData);
				iteration = iteration + 1;
			}			
		}
			
		return testDataIterations;
	  }
	
	/**********************************************************************************************
	* To update the test data file
	*   	
	* @param className {@link String} - Class name
	* @param colName {@link String} - Column name
	* @param value {@link Object} - Value to be updated in excel	
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static void setTestData(String className, String colName, Object value) {
		FileOutputStream fout=null;
		
		String sheetName = getModuleName(className);
	    String testCaseId = getTestCaseId(className);
	    
		setExcelFile(Config.TEST_DATA_FILE_PATH, sheetName);
		int rowNo = getFirstRowContainsTestCaseId(testCaseId, 0);
		int colNo = getColContains(colName);
		setCellData(rowNo, colNo, value);
		
		try {
				fout = new FileOutputStream(new File(Config.TEST_DATA_FILE_PATH));
				excelWBook.write(fout);
				fout.close();
				
		} catch (Exception e) {		
			//TO DO
		}		
	}	
	
	/**********************************************************************************************
	* To get the row no of DB query
	*   	
	* @param queryId {@link String} - Query ID
	* @param colNo {@link int} - Column no
	* @return rowNo {@link int} - Row no
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static int getRowContainsQueryName(String queryId, int colNo){
		int rowNo;
		int rowCount = excelWSheet.getLastRowNum();
		
		for (rowNo=1;rowNo<=rowCount;rowNo++)
			if (getCellData(rowNo, colNo).equalsIgnoreCase(queryId))
				break;
	
		return rowNo;
	}

	/**********************************************************************************************
	* To get the DB query from data DBQueries sheet
	*   	
	* @param queryId {@link String} - Query Id
	* @return query {@link String} - Query
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/	
	public static String getQuery(String queryId) {			
		int queryRow;
		
		setExcelFile(Config.TEST_DATA_FILE_PATH, "DBQueries");
		queryRow = getRowContainsQueryName(queryId, 0);		
		
		return getCellData(queryRow, 1);
	}
}