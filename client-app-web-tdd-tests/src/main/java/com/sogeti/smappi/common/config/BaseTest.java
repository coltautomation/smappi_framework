package com.sogeti.smappi.common.config;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.sogeti.smappi.common.utils.AllureManagerUtil;
import com.sogeti.smappi.common.utils.CapabilityFactoryUtil;
import com.sogeti.smappi.common.utils.DriverManagerUtil;
/**
 * BaseTest class for setup
 * @author Savita Tambe
 *
 */
public class BaseTest { 		
	
	/**********************************************************************************************
	* To start the driver instance
	*   
	* @param context {@link ITestContext} - Testcontext object
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/
	@BeforeTest
	public void testSetUp(ITestContext context) {
		TestRun.setEnvironment(context.getCurrentXmlTest().getParameter("environment"));			
		TestRun.setPlatformName(context.getCurrentXmlTest().getParameter("platformName"));		
		TestRun.setPlatformVersion(context.getCurrentXmlTest().getParameter("platformVersion"));
		TestRun.setBrowserName(context.getCurrentXmlTest().getParameter("browserName"));
		TestRun.setBrowserVersion(context.getCurrentXmlTest().getParameter("browserVersion"));					
		//Sauce Labs
		TestRun.setName(context.getCurrentXmlTest().getParameter("name"));				
	}
	
	/**********************************************************************************************
	* To start the driver instance
	*   
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/
	@BeforeMethod
	public void methodSetUp() {	
		CapabilityFactoryUtil.initiateDriver();					
	}
	
	/**********************************************************************************************
	* To start the driver instance
	*   
	* @author Savita Tambe created May 21, 2018
	* @version 1.0 May 21, 2018
	***********************************************************************************************/
	@AfterMethod
	public void testTearDown() {
		if (Config.SCREENSHOT_CAPTURE.equalsIgnoreCase("LastStep"))
			AllureManagerUtil.attachScreenshot();
			
		DriverManagerUtil.stopWebDriver();
	}
}