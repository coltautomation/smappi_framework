package com.sogeti.smappi.common.utils;

import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.sogeti.smappi.common.config.Config;
import com.sogeti.smappi.common.config.TestRun;
/**
 * Class to setup Driver instances
 * @author Savita Tambe
 *
 */
public class DriverManagerUtil {
	private static ThreadLocal<RemoteWebDriver> webDriver = new ThreadLocal<RemoteWebDriver>();

	 
	 /**********************************************************************************************
     * Sets the Remote WebDriver instance for the running session.
     * 
     * @param remoteWebDriver {@link RemoteWebDriver} - The instance of the driver
     * @author Savita Tambe created March 27, 2018
     * @version 1.0 March 27, 2018
     ***********************************************************************************************/
    public static void setWebDriver(RemoteWebDriver remoteWebDriver) {
        webDriver.set(remoteWebDriver);
    }
    
    /**********************************************************************************************
     * Gets the Remote WebDriver instance for the running session.
     *      
     * @return driver {@link RemoteWebDriver} - The instance of the driver
     * @author Savita Tambe created March 27, 2018
     * @version 1.0 March 27, 2018
     ***********************************************************************************************/
    public static RemoteWebDriver getWebDriver() {
        return webDriver.get();
    }
    
    /**********************************************************************************************
	 * Sets the Driver instance for the running session.
	 * 
	 * @param capabilities {@link DesiredCapabilities} - The desired capabilities
	 * @author Savita Tambe created March 27, 2018
	 * @version 1.0 March 27, 2018
	 ***********************************************************************************************/	
	public static void initiateWebDriver(DesiredCapabilities capabilities) { 		
		String url = null;
		String protocal = "http://";
		switch(TestRun.getEnvironment().toUpperCase())
		{
		   case "LOCAL" :
		   {
				url = protocal + Config.GRID_HUB_IP + ":" + Config.GRID_HUB_PORT + "/wd/hub";
				break;
		   }
		   
		   case "BROWSERSTACK" :
		   {
				url = protocal + Config.BROWSER_STACK_USER_NAME + ":" + Config.BROWSER_STACK_ACCESS_KEY + Config.BROWSER_STACK_URL;
				break;
		   }
			
		   case "SAUCELABS" :
		   {
				url = protocal + Config.SAUCELABS_USER_NAME + ":" + Config.SAUCELABS_ACCESS_KEY + Config.SAUCELABS_URL;
				break;
		   }
		 
		   case "PERFECTO" :
		   {
		  		url = Config.PERFECTO_URL + "/fast";
		  		break;
		   }
		  		
		   case "SEETEST" :
		   {
				url = Config.SEETEST_URL;
				break;
		   }	
		   
		   default:
			   url = protocal + Config.GRID_HUB_IP + ":" + Config.GRID_HUB_PORT + "/wd/hub";
		}	
        	
		webDriver(url, capabilities);
	}

	
	/**********************************************************************************************
     * Set Web Driver instance for the running session.
     * 
     * @param url {@link String} - The url
     * @param capabilities {@link DesiredCapabilities} - The desired capabilities
     * @author Savita Tambe created March 27, 2018
     * @version 1.0 March 27, 2018
     ***********************************************************************************************/
	public static void webDriver(String url, DesiredCapabilities capabilities) {
		 try {
			 	setWebDriver(new RemoteWebDriver(new URL(url), capabilities));
	     } catch (Exception e) {
	         LoggerUtil.logErrorMessage("Session could not be created: " + e);
	     }
	     
	     if (webDriver == null) {
	         LoggerUtil.logErrorMessage("The driver was not properly initiated.");
	     }   
    }

	/**********************************************************************************************
     * Stops Web Driver instance for the running session.
     * 
     * @author Savita Tambe created March 27, 2018
     * @version 1.0 March 27, 2018
     ***********************************************************************************************/
	public static void stopWebDriver() {
		try {
		 		getWebDriver().close();
			} catch (Exception e) {}
				 
		try {
				getWebDriver().quit();
		} catch (Exception e) {}
				
		LoggerUtil.logConsoleMessage("Browser closed.");
	}
	
	/**********************************************************************************************
     * Start Selenium Grid
     * 
     * @author Savita Tambe created March 27, 2018
     * @version 1.0 March 27, 2018
     ***********************************************************************************************/
	public static void startSeleniumGrid() {
		try { 
				Runtime.getRuntime().exec("cmd /c start " + Config.SELENIUM_GRID_FILE_PATH); 
	 	} 
		catch (Exception e) {
			LoggerUtil.logErrorMessage("Could not start Selenium grid: " + e);				 
		}
		
	}
}