package com.client.app.uitests.pageobjects;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.sogeti.smappi.common.utils.DriverManagerUtil;


public class OrangeHRMPage {
	//Login Page
	@FindBy(id = "txtUsername")
	public WebElement userNameTxb;
	
	@FindBy(id = "txtPassword")
	public WebElement passwordTxb;
	
	@FindBy(id = "btnLogin")
	public WebElement loginBtn;
	
	//Home Page
	@FindBy(id = "welcome")
	public WebElement welcomeUserLnk;
	
	@FindBy(id = "menu_admin_viewAdminModule")
	public WebElement adminLnk;
	
	@FindBy(id = "menu_admin_UserManagement")
	public WebElement userManagementLnk;
	
	@FindBy(id = "menu_admin_viewSystemUsers")
	public WebElement usersLnk;
	
	//Add Users	
	@FindBy(id = "systemUser-information")
	public WebElement systemUserInfoFrm;
	
	@FindBy(id = "searchBtn")
	public WebElement searchBtn;
	
	@FindBy(id = "btnAdd")
	public WebElement addBtn;
	
	@FindBy(css = ".left>a")
	public List<WebElement> employeeNameLst;
	
	@FindBy(id = "systemUser_userType")
	public WebElement userRoleLst;
	
	@FindBy(id = "systemUser_employeeName_empName")
	public WebElement employeeNameTxb;
	
	@FindBy(id = "systemUser_userName")
	public WebElement addUserNameTxb;
	
	@FindBy(id = "systemUser_status")
	public WebElement statusLst;
	
	@FindBy(id = "systemUser_password")
	public WebElement newPasswordTxb;
	
	@FindBy(id = "systemUser_confirmPassword")
	public WebElement confirmPasswordTxb;
	
	@FindBy(id = "btnSave")
	public WebElement saveBtn;
	
	
	//Add Job	
	@FindBy(id = "menu_admin_Job")	
	public WebElement jobLnk;
	
	@FindBy(id = "menu_admin_viewJobTitleList")	
	public WebElement jobTitlesFrm;
	
	@FindBy(id = "jobTitleList")
	public WebElement jobTitleLnk;
	
	@FindBy(id = "jobTitle_jobTitle")	
	public WebElement jobTitleTxb;
	
	@FindBy(id = "jobTitle_jobDescription")
	public WebElement jobDescriptionTxb;
	
	@FindBy(id = "jobTitle_note")
	public WebElement jobNoteTxb;
	
	@FindBy(id = "btnDelete")
	public WebElement deleteBtn;
	
	@FindBy(id = "dialogDeleteBtn")
	public WebElement dialogDeleteBtn;
	
	//Work shifts
	@FindBy(id = "menu_admin_workShift")
	public WebElement workShiftsLnk;
	
	@FindBy(id = "search-results")
	public WebElement workShiftFrm;
	
	@FindBy(id = "workShift_name")
	public WebElement shiftNameTxb;
	
	@FindBy(id = "workShift_workHours_from")
	public WebElement workFromTxb;
	
	@FindBy(id = "workShift_workHours_to")
	public WebElement workToTxb;
	
	@FindBy(id = "workShift_availableEmp")
	public WebElement availableEmployeesLst;
	
	@FindBy(id = "btnAssignEmployee")
	public WebElement assignEmp;
	
	@FindBy(xpath = "//a[@href='/index.php/auth/logout']")
	public WebElement logoutLnk;
	
	
	public OrangeHRMPage() {  
		PageFactory.initElements(DriverManagerUtil.getWebDriver(), this);		
	}
}
