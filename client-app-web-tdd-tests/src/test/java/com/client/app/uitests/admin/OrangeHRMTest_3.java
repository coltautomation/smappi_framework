package com.client.app.uitests.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.client.app.uitests.utils.OrangeHRMUtil;
import com.sogeti.smappi.common.config.BaseTest;
import com.sogeti.smappi.common.utils.ExcelUtil;


public class OrangeHRMTest_3 extends BaseTest {
	protected static String className;	
	protected static HashMap<Integer, HashMap<String, String>> testData;
	protected OrangeHRMUtil orangeHRMUtil;	

	
	@BeforeMethod
	public void setup() {		 
		orangeHRMUtil = new OrangeHRMUtil();
	}
		
	@DataProvider(name = "getData")
	public Iterator<Object[]> getTestData() {
		className = this.getClass().getSimpleName();
		testData = ExcelUtil.getTestData(className);

		ArrayList<Object[]> dataProvider = new ArrayList<Object[]>();
		for (Integer currentKey : testData.keySet()) {
			dataProvider.add(new Object[] { testData.get(currentKey) });
		}

		return dataProvider.iterator();
	}

	//@Test(dataProvider = "getData", retryAnalyzer = TestListeners.class) - To execute failed test script one more time
	@Test(dataProvider = "getData") 
	public void verifyShiftTest(HashMap<String, String> testData) {		
		boolean status = orangeHRMUtil.loginToOrangeHRM(testData);
        Assert.assertTrue(status, "Login failed.");

		status = orangeHRMUtil.naviteToWorkShiftsModule();
        Assert.assertTrue(status, "Work shift module navigation failed.");

		status = orangeHRMUtil.setWorkShiftDetails(testData);
        Assert.assertTrue(status, "Work shift not saved.");

		status = orangeHRMUtil.deleteWorkShift(testData);
        Assert.assertTrue(status, "Work shift not deleted.");	        		 		
	}
}
