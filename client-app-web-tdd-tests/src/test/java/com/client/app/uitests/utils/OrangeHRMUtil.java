package com.client.app.uitests.utils;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.client.app.uitests.pageobjects.OrangeHRMPage;
import com.sogeti.smappi.common.config.Config;
import com.sogeti.smappi.common.utils.DriverManagerUtil;
import com.sogeti.smappi.common.utils.LoggerUtil;
import com.sogeti.smappi.common.utils.WebInteractUtil;
import io.qameta.allure.Step;


public class OrangeHRMUtil {
		protected OrangeHRMPage orangeHRMPage;

		public OrangeHRMUtil() {
			orangeHRMPage = new OrangeHRMPage();
		}
		
		@Step("Login to employee management application")
		public boolean loginToOrangeHRM(Map<String, String> testData) {
			boolean status = false;
	
			WebInteractUtil.launchWebApp(Config.APP_URL);
			logOutOrangeHRM();
			
			if (WebInteractUtil.isPresent(orangeHRMPage.userNameTxb, Config.SMALL_PAUSE)) {
				WebInteractUtil.sendKeys(orangeHRMPage.userNameTxb, testData.get("UserName"));
				WebInteractUtil.sendKeys(orangeHRMPage.passwordTxb, testData.get("Password"));
				WebInteractUtil.clickByJS(orangeHRMPage.loginBtn);
	
				if (WebInteractUtil.isPresent(orangeHRMPage.welcomeUserLnk, Config.SMALL_PAUSE)) {
					LoggerUtil.logMessage("User login successfull.");
					status = true;
				}
				else 
					LoggerUtil.logMessage("User login unsuccessfull.");
			}
			else 
				LoggerUtil.logMessage("Application is not loaded.");
				
			return status;
		}
		
		@Step("Navigate to Users module")
		public boolean naviteToUsersModule() {
			boolean status = false;
					
			WebInteractUtil.click(orangeHRMPage.adminLnk);
			WebInteractUtil.clickByJS(orangeHRMPage.userManagementLnk);
			WebInteractUtil.clickByJS(orangeHRMPage.usersLnk);
						
			if (WebInteractUtil.isPresent(orangeHRMPage.systemUserInfoFrm, Config.SMALL_PAUSE)) {
				LoggerUtil.logMessage("Navigated to Users module successfully.");
				status = true;
			}
			else 
				LoggerUtil.logMessage("Could not navigate to Users module.");
			
			return status;
		}
		
		@Step("Set user details")
		public boolean setUserDetails(Map<String, String> testData) {
			boolean status = false;
				
			List<WebElement> employeeNameLst = orangeHRMPage.employeeNameLst;
			String employeeName = employeeNameLst.get(0).getText().split("\\.")[0];
			
			WebInteractUtil.click(orangeHRMPage.addBtn);
			WebInteractUtil.selectByVisibleText(orangeHRMPage.userRoleLst, testData.get("UserRole"));
			WebInteractUtil.sendKeys(orangeHRMPage.employeeNameTxb, employeeName);	
			orangeHRMPage.employeeNameTxb.sendKeys(Keys.TAB);
			WebInteractUtil.sendKeys(orangeHRMPage.addUserNameTxb, testData.get("AddUserName"));
			
			WebInteractUtil.selectByVisibleText(orangeHRMPage.statusLst, testData.get("Status"));
			WebInteractUtil.sendKeys(orangeHRMPage.newPasswordTxb, testData.get("NewPassword"));
			WebInteractUtil.sendKeys(orangeHRMPage.confirmPasswordTxb, testData.get("ConfirmPassword"));			
			LoggerUtil.logMessage("User details set successfully.");
			
			WebInteractUtil.clickByJS(orangeHRMPage.saveBtn);
						
			if (WebInteractUtil.isPresent(orangeHRMPage.searchBtn, Config.SMALL_PAUSE)) {
				LoggerUtil.logMessage("Users added successfully.");
				status = true;
			}
			else 
				LoggerUtil.logMessage("Users not added successfully.");
			
			return status;
		}
		
		@Step("Navigate to Jobs module")
		public boolean naviteToJobsModule() {
			boolean status = false;
					
			WebInteractUtil.click(orangeHRMPage.adminLnk);
			WebInteractUtil.click(orangeHRMPage.jobLnk);
			WebInteractUtil.clickByJS(orangeHRMPage.jobTitlesFrm);
						
			if (WebInteractUtil.isPresent(orangeHRMPage.jobTitleLnk, Config.SMALL_PAUSE)) {
				LoggerUtil.logMessage("Navigated to Jobs module successfully.");
				status = true;
			}
			else 
				LoggerUtil.logMessage("Could not navigate to Jobs module.");
			
			return status;
		}
		
		@Step("Set Job details")
		public boolean setJobDetails(Map<String, String> testData) {
			boolean status = false;
			int size = 0;
			
			WebInteractUtil.click(orangeHRMPage.addBtn);
			WebInteractUtil.sendKeys(orangeHRMPage.jobTitleTxb, testData.get("JobTitle"));
			WebInteractUtil.sendKeys(orangeHRMPage.jobDescriptionTxb, testData.get("JobDescription"));
			WebInteractUtil.sendKeys(orangeHRMPage.jobNoteTxb, testData.get("JobNote"));
			LoggerUtil.logMessage("Job details set successfully.");

			WebInteractUtil.clickByJS(orangeHRMPage.saveBtn);
			WebInteractUtil.waitForElementToBeVisible(orangeHRMPage.addBtn, Config.SMALL_PAUSE);
			
			if (WebInteractUtil.isPresent(orangeHRMPage.saveBtn, Config.SMALL_PAUSE)) 
				LoggerUtil.logMessage("Job not added successfully.");
			else {
					size = DriverManagerUtil.getWebDriver().findElementsByLinkText(testData.get("JobTitle")).size();				
					if (size > 0) {
						LoggerUtil.logMessage("Job added successfully.");
						status = true;
					}
					else 
						LoggerUtil.logMessage("Job not added successfully.");
			}
			
			return status;
		}
		
		@Step("Delete Job")
		public boolean deleteJob(Map<String, String> testData) {
			boolean status = false;
			int size = 0;
					
			DriverManagerUtil.getWebDriver().findElementByXPath("//a[text()='" + testData.get("JobTitle") + "']/parent::td/preceding-sibling::td/input").click();					
			WebInteractUtil.click(orangeHRMPage.deleteBtn);
			LoggerUtil.logMessage("Job selected for deletion.");

			WebInteractUtil.click(orangeHRMPage.dialogDeleteBtn);
			size = DriverManagerUtil.getWebDriver().findElementsByLinkText(testData.get("JobTitle")).size();
			
			if (size == 0) {
				LoggerUtil.logMessage("Job deleted successfully.");
				status = true;
			}
			else 
				LoggerUtil.logMessage("Job not deleted successfully.");
			
			return status;
		}
	
		@Step("Navigate to Work Shifts module")
		public boolean naviteToWorkShiftsModule() {
			boolean status = false;
					
			WebInteractUtil.click(orangeHRMPage.adminLnk);
			WebInteractUtil.clickByJS(orangeHRMPage.jobLnk);
			WebInteractUtil.clickByJS(orangeHRMPage.workShiftsLnk);
						
			if (WebInteractUtil.isPresent(orangeHRMPage.workShiftFrm, Config.SMALL_PAUSE)) {
				LoggerUtil.logMessage("Navigated to Work Shifts module successfully.");
				status = true;
			}
			else 
				LoggerUtil.logMessage("Could not navigate to Work Shifts module.");
			
			return status;
		}
		
		@Step("Set Work Shift details")
		public boolean setWorkShiftDetails(Map<String, String> testData) {
			boolean status = false;
			int size = 0;
			
			WebInteractUtil.click(orangeHRMPage.addBtn);
			WebInteractUtil.sendKeys(orangeHRMPage.shiftNameTxb, testData.get("ShiftName"));
			WebInteractUtil.selectByVisibleText(orangeHRMPage.workFromTxb, testData.get("WorkHoursFrom"));
			WebInteractUtil.selectByVisibleText(orangeHRMPage.workToTxb, testData.get("WorkHoursTo"));
			
			for (int i =0;i<5;i++)
				WebInteractUtil.selectByIndex(orangeHRMPage.availableEmployeesLst, i);
			
			WebInteractUtil.clickByJS(orangeHRMPage.assignEmp);
			LoggerUtil.logMessage("Work shift details set successfully.");

			WebInteractUtil.clickByJS(orangeHRMPage.saveBtn);
			WebInteractUtil.waitForElementToBeVisible(orangeHRMPage.addBtn, Config.SMALL_PAUSE);
			
			size = DriverManagerUtil.getWebDriver().findElementsByLinkText(testData.get("ShiftName")).size();			
			if (size > 0) {
				LoggerUtil.logMessage("Shift added successfully.");
				status = true;
			}
			else 
				LoggerUtil.logMessage("Shift not added successfully.");
			
			return status;
		}
		
		@Step("Delete Work Shift")
		public boolean deleteWorkShift(Map<String, String> testData) {
			boolean status = false;
			int size = 0;
					
			DriverManagerUtil.getWebDriver().findElementByXPath("//a[text()='" + testData.get("ShiftName") + "']/parent::td/preceding-sibling::td/input").click();				
			WebInteractUtil.click(orangeHRMPage.deleteBtn);
			LoggerUtil.logMessage("Work shift selected for deletion.");

			WebInteractUtil.click(orangeHRMPage.dialogDeleteBtn);
			size = DriverManagerUtil.getWebDriver().findElementsByLinkText(testData.get("ShiftName")).size();
			
			if (size == 0) {
				LoggerUtil.logMessage("Work shift deleted successfully.");
				status = true;
			}
			else 
				LoggerUtil.logMessage("Work shift not deleted successfully.");
			
			return status;
		}
		
		@Step("Logout of employee management application")
		public boolean logOutOrangeHRM() {
			boolean status = false;
			
			if (WebInteractUtil.isPresent(orangeHRMPage.welcomeUserLnk, Config.XSMALL_PAUSE)) {
				WebInteractUtil.click(orangeHRMPage.welcomeUserLnk);	
				WebInteractUtil.clickByJS(orangeHRMPage.logoutLnk);							
	
				if (WebInteractUtil.isPresent(orangeHRMPage.loginBtn, Config.SMALL_PAUSE)) {
					status = true;
				}
			}
			
			return status;
		}	
}
