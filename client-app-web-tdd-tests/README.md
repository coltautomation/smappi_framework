SMAppi
	Sogeti Selenium Automation Framework
	
Overview
	This is desktop web automation framework using Selenium and test runner like TestNG. 
	The 3rd party reporting API Allure is used to improve the report presentation. 
	The framework adapts the modular and data-driven concepts. 
	The entire automation suite can be stored in customers private Git/BitBucket and uses Maven build tool to build and run the tests on browsers. 
	This is integrated with CI tools like Jenkins.
	In the end, it create Allure and TestNg reports.

Features
	Supports Multiple OS 
	Supports Multiple browsers
	Supports Multiple Cloud platforms	
	Supports Parallel execution
	Supports Parameterization with Excel
	Supports Multiple Iterations
	Supports Build Automation with Maven
	Supports Screenshot configuration
	Provides generic utilities
	Provides Standard Allure reports
	Provide Customized TestNG Reports
	Provide logs
	Automatic code review
	Easier, faster and efficient analysis of result	
	Javadoc plugin

Pre-requisites
	JDK 1.8 should be installed on system
			
Implementation
	Please refer below user guide for the implemetation
		src/main/resources/docs/Selenium Automation Framework User Guide.doc

SonarQube Dashboard
	Launch Sonarqube from <Location>\sonarqube-<version>\bin\windows-x86-32\StartSonar.bat
			
About 
	 This Selenium based framework is licensed under the terms of the Apache 2.0 License. Please refer LICENSE file
	 
License
	Apache 2.0 License